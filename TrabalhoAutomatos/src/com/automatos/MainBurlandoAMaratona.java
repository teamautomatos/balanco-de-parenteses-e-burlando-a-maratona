package com.automatos;

import java.util.HashMap;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Stack;
import java.util.regex.*;

public class MainBurlandoAMaratona {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String regex;
        int numExp;

        while(true) {
            numExp = 0;
            regex = "\\(";
            while(!isBalanced(regex)) {
                System.out.println("Digite um regex");
                regex = sc.next();
                if(!isBalanced(regex)) System.out.println("Regex inválida.");
            }

            if (regex.contains(".")) {
                int index = regex.indexOf(".");
                regex = regex.substring(0, index) + regex.substring(index + 1, regex.lastIndexOf(")") + 1);
            }

            boolean validNumber = true;
            while(validNumber) {
                validNumber = true;
                try {
                    System.out.println("Digite um numero inteiro");
                    numExp = sc.nextInt();
                } catch (NumberFormatException e) {
                    validNumber = false;
                    System.err.println("Número inteiro inválido, digite outro.");
                } finally {
                    if(validNumber == false)
                        continue;
                    else
                        break;
                }
            }

            ArrayList<Character> results = new ArrayList<Character>();
            for (int i = 0; i < numExp; i++) {
                String expression = sc.next();
                if (expression.matches(regex))
                    results.add('Y');
                else
                    results.add('N');
            }
            // imprimir
            results.forEach(res -> {
                System.out.println(res);
            });
            System.out.print(""); // Linha em branco pós hipóteses
            results.clear();
        }
    }

    public static boolean isBalanced(String expression) {
        Stack<Character> stack = new Stack<>();
        for (char c: expression.toCharArray()) { // Leitura da string
            switch(c) {
                case '(':
                    stack.push(c);
                    break;
                case ')':
                    if(stack.isEmpty())
                        return false;
                    else
                        stack.pop();
                    break;
            }
        }
        return stack.isEmpty();
    }

    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
}

