package com.automatos;

import java.util.Stack;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class MainBalancoParenteses {

    static final String fileName = "expressions.dat";

    static ArrayList<String> expressions = new ArrayList<>();

    public static void main(String[] args) {
        try {
            System.out.println("Lendo expressões do arquivo " + fileName);
            carregarExpressoes();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Houve um erro carregando o arquivo " + fileName);
        }
        System.out.format("%sEntrada  %s--> %sSaida\n", ANSI_BLACK_BACKGROUND+ANSI_PURPLE, ANSI_BLACK_BACKGROUND+ANSI_YELLOW, ANSI_BLACK_BACKGROUND+ANSI_BLUE);
        expressions.forEach(s -> {
            System.out.format(ANSI_BLACK_BACKGROUND+ANSI_PURPLE+"%s \t%s-->\t %s\n", s, ANSI_BLACK_BACKGROUND+ANSI_YELLOW, ANSI_BLACK_BACKGROUND+(isBalanced(s) ? ANSI_GREEN+"Correto" : ANSI_RED+"Incorreto"));
        });
    }


    public static boolean isBalanced(String expression) {
        Stack<Character> stack = new Stack<>();
        for (char c: expression.toCharArray()) { // Leitura da string
            switch(c) {
                case '(':
                    stack.push(c);
                    break;
                case ')':
                    if(stack.isEmpty())
                        return false;
                    else
                        stack.pop();
                    break;
            }
        }
        return stack.isEmpty();
    }

    public static void carregarExpressoes() throws IOException {
        Path path2 = Paths.get(fileName);
        try (Scanner sc = new Scanner(Files.newBufferedReader(path2, Charset.forName("utf8")))) {
            sc.useDelimiter("[\n]");
            while (sc.hasNextLine()) {
                String e = sc.nextLine().replace("\r", "");
                expressions.add(e);
                //System.out.println(e);
            }
        }
        catch (IOException x) {
            System.err.format("Erro de E/S: %s%n", x);
        }
    }

    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
}

